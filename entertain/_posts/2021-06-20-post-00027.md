---
layout: post
title: "넷플릭스 드라마 추천 8편"
toc: true
---


## 넷플릭스 드라마 추천
 

 

 

 

 

### 넷플릭스 드라마 추천1 - 킹덤
 병든 왕을 둘러싸고 흉흉한 소문이 떠돈다. 어둠에 뒤덮인 조선, 기이한 역병에 신음하는 산하. 밑자리 모를 악에 맞서 백성을 구원할 희망은 전혀 세자뿐이다. 김성훈 감독과 김은희 작가가 만난 스릴러 드라마입니다.
 전세계적으로 흥행한 우리나라 평판 드라마라는 생각이 듭니다. TV 한도 극 작품상을 포함해 백상예술대상 2개 경지 후보에 올랐는데요 추후 시즌이 이렇게 기다려지는 드라마는 처음인 것 같습니다.
 

 

 

 

 

### 넷플릭스 희곡 추천2 - 종이의집
 1명의 천재, 8명의 공범, 철저히 준비한 세기의 강도. 스페인 조폐국에서 인질극까지 벌인 이들은 진짜로 포위 경찰을 따돌리고 거액의 돈과 다름없이 달아날 운 있을까? 국제에미상과 페닉스상 드라마 시리즈 범주 작품상 수상.
 이리스상 최우수 드라마 시리즈를 비롯해 7개의 트로피를 거머쥔 작품이기도 한데요. 개인적으로 청소년관람불가 드라마는 어느정도 재미가 보장이 된다는 입장으로서 종이의집을 추천드리고 싶습니다.
 

 

 

 

 

### 넷플릭스 드라마 추천3 - 워킹데드
 눈을 떠보니 세상은 좀비가 점령한 전쟁터. 어디로 갈 것인가, 어떻게 생존할 것인가. 현실이 지옥일 물정 희망은 의미가 있는가. 2010년 첫 시즌을 시작으로 역대 케이블 극 지려 최고 시청률을 기록.
 우리나라에 좀비 희곡 킹덤이 있다면 미국에는 워킹데드가 있는데요 긴 시즌과 시청률 작성 등을 생각해보면 절대로 재미가 없을 삶 없는 넷플릭스 극 추천작 입니다.
 

 

 

 

 

### 넷플릭스 드라마 추천4 - 심야식당
 문뜩 보면 평범해 보이는 제한 성질 식당의 단골손님들은 특정한 음식에 대한 공통의 애정을 바탕으로 쌍방 간에 단순하지만 깊은 유대관계를 쌓아 나간다. 제55회 일반만화 한계 쇼가쿠칸 만화상 수상작 深夜食堂(신야쇼쿠도).
 국내에서 번역본만 43만부가 팔린 만화책을 원작으로 제작한 극 시리즈이다. 본성 특유의 감성을 느낄 고갱이 있으며 저마다의 사연을 가진 손님들의 스토리를 통해 감동과 재미를 전하는 드라마입니다.
 

 

 

 

 

### 넷플릭스 드라마 추천5 - 루머의 루머의 루머
 [영화 다시보기](https://imagetowebp.com/entertain/post-00002.html) 친구의 비극적인 척살 후, 미스터리한 일들이 연이어 일어난다. 의사 아픈 사건들의 중심에 서는 클레이 젠슨. 고등학생이 감당하기에는 영 힘겨운 시간을 보낸다. 사회적으로 커다란 반향을 일으킨 시리즈.
 주변에서 추천하는 사람들이 많은데 저도 아직 여성 본 넷플릭스 극 추천작 입니다. 해나 베이커 역의 캐서린 랭퍼드는 소득 작품으로 골든글로브 후보에 올랐다. 제이 애셔의 베스트셀러 사담 원작입니다.
 

 

 

 

 

### 넷플릭스 극 추천6 - 셜록
 아서 코넌 도일 경의 사람됨 사고 소설을 새롭게 재해석한 작품. 괴짜 명탐정 셜록이 단서를 찾아 현대의 런던 거리를 배회한다. 베너딕트 컴버배치, 마틴 프리먼, 우나 스텁스 주연.
 TV 영화로 먼저 방영된 시즌 3의 '유령신부' 에피소드는 에미상 2관왕에 올랐다. 저는 어릴때부터 명탐정 코난 등 추리만화를 좋아했는데요 어느정도 취향을 재해 것 같습니다.
 

 

 

 

 

### 넷플릭스 희곡 추천7 - 오렌지 이즈 훨씬 뉴 블랙
 상류층 뉴요커가 과거의 범죄에 발목을 잡힌다. 그녀가 여성 교도소에 수감되면서 벌어지는 일들을 그린 시리즈. 드라마 《위즈》의 제작자가 내놓은 에미상 수상작이다.
 우조 아두바는 '크레이지 아이'역으로 드라마(2015)와 코미디(2014) 부문에서 모두 에미상을 수상하는 기염을 토했다.
 

 

 

 

 

### 넷플릭스 드라마 추천8 - 빨간 소감 앤
 마음에 불꽃을 품은 여자아이 앤. 나이 든 오누이의 초록지붕집에 발을 들이게 된다. 커스버트 남매와 낯선 고아 소녀가 가족이 되고 성장하는 과정을 그려낸 시리즈.
 《브레이킹 배드》로 에미상 수상에 빛나는 작가이자 프로듀서 모이라 월리-베킷이 루시 모드 몽고메리의 고전을 새롭게 해석했다.
 

 

 

 

 

 이렇게 해서 총 8편의 넷플릭스 희곡 추천을 해봤습니다. 취향에 따라 어느정도 호불호가 갈릴 아주 있지만 전세계적으로 인기있는 작품들이니 재미있게 보실 명 있을 겁니다.
 어디까지나 개인적으로 재미있는 작품들일 뿐이며 포스팅 순위가 추천 순위가 아니라 그저 무작위로 쓴거기 때문에 8개의 넷플릭스 극 추천 제품 중에서 마음에 드는걸 보시면 되겠습니다.
